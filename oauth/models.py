import math
from datetime import timedelta

from django.db import models
from django.urls import reverse
from django.utils import timezone
from requests_oauthlib import OAuth2Session


class BaseOAuthModelManager(models.Manager):
    def get_login_url(self, request) -> str:
        redirect_uri = request.build_absolute_uri(reverse(
            f'admin:{self.model._meta.app_label}_{self.model._meta.model_name}_oauth_callback'
        ))
        url, state = OAuth2Session(self.model.client_id,
                                   redirect_uri=redirect_uri,
                                   scope=self.model.scope). \
            authorization_url(self.model.auth_url)

        return url

    def create_from_oauth_callback(self, request) -> 'BaseOAuthModel':
        redirect_uri = request.build_absolute_uri(reverse(
            f'admin:{self.model._meta.app_label}_{self.model._meta.model_name}_oauth_callback'
        ))
        authorization_code = request.GET.get('code')

        session = OAuth2Session(self.model.client_id, redirect_uri=redirect_uri, scope=self.model.scope)

        token = session.fetch_token(
            self.model.refresh_url,
            code=authorization_code,
            client_id=self.model.client_id,
            client_secret=self.model.client_secret
        )

        expires_at = timezone.now() + timedelta(seconds=token['expires_in'])

        return self.model(
            access_token=token['access_token'],
            refresh_token=token['refresh_token'],
            expires_at=expires_at,
            token_type=token['token_type']
        )


class BaseOAuthModel(models.Model):
    class Meta:
        abstract = True

    refresh_token = models.TextField()
    access_token = models.TextField()
    expires_at = models.DateTimeField()
    token_type = models.TextField()

    objects = BaseOAuthModelManager()

    _session = None

    def update_token(self, token):
        self.access_token = token['access_token']
        self.refresh_token = token['refresh_token']
        self.expires_at = timezone.now() + timedelta(seconds=token['expires_in'])
        self.token_type = token['token_type']

        return self.save(update_fields=[
            'access_token', 'refresh_token', 'expires_at', 'token_type'
        ], force_update=True)

    def to_dict(self) -> dict:
        return {
            'access_token': self.access_token,
            'refresh_token': self.refresh_token,
            'expires_in': math.floor((self.expires_at - timezone.now()).total_seconds()),
            'token_type': self.token_type,
        }

    @property
    def session(self) -> OAuth2Session:
        if not self._session:
            extra = {
                'client_id': self.client_id,
                'client_secret': self.client_secret
            }
            self._session = OAuth2Session(client_id=self.client_id,
                                          token=self.to_dict(),
                                          auto_refresh_url=self.refresh_url,
                                          auto_refresh_kwargs=extra,
                                          token_updater=self.update_token,
                                          scope=self.scope)
        return self._session
