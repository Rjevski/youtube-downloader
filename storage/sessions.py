import urllib.parse

from django.conf import settings

from storage.utils import FifoFileBuffer


class UploadSession:
    incremental_chunk_size = 4_000_000
    max_single_upload_size = 4_000_000
    upload_url = None
    cursor = 0
    base_url = settings.MICROSOFT_GRAPH_BASE_URL
    buffer = FifoFileBuffer()
    progress_callbacks = []
    item_id = None

    def __init__(self, path, size, session):
        self.path = path
        self.size = size
        self.session = session

    def _request(self, *args, **kwargs):
        resp = self.session.request(*args, **kwargs)
        resp.raise_for_status()

        return resp

    def _request_json(self, *args, **kwargs):
        return self._request(*args, **kwargs).json()

    def _notify_progress(self, progress_info):
        for callback in self.progress_callbacks:
            callback(progress_info)

    def open_session(self):
        resp = self._request_json(
            "POST",
            f"{self.base_url}/me/drive/special/approot:{urllib.parse.quote(self.path)}:/createUploadSession",
        )

        self.upload_url = resp["uploadUrl"]

    def upload_chunk(self):
        if not self.upload_url:
            self.open_session()

        chunk = self.buffer.read(size=self.incremental_chunk_size)

        resp = self._request(
            "PUT",
            self.upload_url,
            headers={
                "Content-Range": f"bytes {self.cursor}-{self.cursor + len(chunk) - 1}/{self.size}"
            },
            data=chunk,
        )

        if resp.status_code == 201:
            self.item_id = resp.json()["id"]

        self.cursor += len(chunk)

        self._notify_progress(
            {
                "uploaded_bytes": self.cursor,
                "total_bytes": self.size,
                "_percent_str": "%3.1f%%" % (self.cursor / self.size * 100),
            }
        )

    def upload_once(self):
        if self.buffer.available:
            if not self.upload_url:
                content = self.buffer.read(size=self.max_single_upload_size)

                resp = self._request_json(
                    "PUT",
                    f"{self.base_url}/me/drive/special/approot:{self.path}:/content",
                    data=content,
                )

                if resp.status_code == 201:
                    self.item_id = resp["id"]

                self._notify_progress({"current": len(content), "total": self.size})

    def write(self, chunk):
        if len(chunk) + self.cursor > self.size:
            raise IOError("Can't write more than initially defined file size.")

        self.buffer.write(chunk)

        if self.upload_url or self.buffer.available > self.max_single_upload_size:
            if self.buffer.available > self.incremental_chunk_size:
                self.upload_chunk()

    def share(self, **kwargs):
        self.close()

        return self._request(
            "POST",
            f"{settings.MICROSOFT_GRAPH_BASE_URL}/me/drive/items/{self.item_id}/createLink",
            json=kwargs,
        ).json()

    def close(self):
        if self.buffer.available:
            if self.upload_url or self.buffer.available > self.max_single_upload_size:
                while self.buffer.available:
                    self.upload_chunk()
            else:
                self.upload_once()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()
