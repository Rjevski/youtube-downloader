from django.contrib import admin

from oauth.admin import BaseOAuthModelAdmin
from storage.models import OneDrive


# Register your models here.


class OneDriveAdmin(BaseOAuthModelAdmin):
    exclude = ['access_token', 'refresh_token', 'expires_at', 'token_type', 'user']


admin.site.register(OneDrive, OneDriveAdmin)
