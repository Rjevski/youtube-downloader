from django.conf import settings
from django.contrib.auth.models import User
from django.db import models

from oauth.models import BaseOAuthModel, BaseOAuthModelManager


class OneDriveManager(BaseOAuthModelManager):
    def create_from_oauth_callback(self, request) -> 'OneDrive':
        model = super().create_from_oauth_callback(request)
        me = model.session.get(f'{settings.MICROSOFT_GRAPH_BASE_URL}/me').json()

        model.name = me['displayName']
        model.user = request.user

        return model


class OneDrive(BaseOAuthModel):
    class Meta:
        verbose_name = 'OneDrive'

    client_id = settings.MICROSOFT_GRAPH_CLIENT_ID
    client_secret = settings.MICROSOFT_GRAPH_CLIENT_SECRET
    refresh_url = 'https://login.microsoftonline.com/common/oauth2/v2.0/token'
    auth_url = 'https://login.microsoftonline.com/common/oauth2/v2.0/authorize'
    scope = 'https://graph.microsoft.com/.default offline_access'

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='onedrive')
    name = models.TextField()

    objects = OneDriveManager()

    def __str__(self):
        return self.name
