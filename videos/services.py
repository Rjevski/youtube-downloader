import feedparser
from django.utils import timezone

from videos.models import Subscription, Video
from videos.tasks import download


def refresh_subscription(subscription: Subscription):
    check_time = timezone.now()

    feed = feedparser.parse(subscription.url)
    new_entries = filter(lambda x: x.published_parsed > subscription.last_checked.utctimetuple(), feed.entries)

    for entry in new_entries:
        video = Video.objects.create_from_url(url=entry.link, user=subscription.user)
        download_video(video)

    subscription.last_checked = check_time
    subscription.save()


def download_video(video: Video):
    result = download.delay(video.id)

    video.task_id = result.task_id
    video.save()

    return result
