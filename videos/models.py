import logging

import feedparser
import youtube_dl
from django.contrib.auth.models import User
from django.db import models

from videos.validators import validate_youtube_url

# Create your models here.

logger = logging.getLogger(__name__)


class SubscriptionManager(models.Manager):
    def instantiate_from_url(self, url: str, **kwargs):
        url = validate_youtube_url(url)
        parsed = feedparser.parse(url)

        return self.model(title=parsed.feed.title, url=url, **kwargs)


class Subscription(models.Model):
    class Meta:
        unique_together = ('url', 'user')

    title = models.TextField(blank=True)
    url = models.URLField("URL", validators=[validate_youtube_url])
    last_checked = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(
        User, related_name="subscriptions", on_delete=models.CASCADE
    )

    objects = SubscriptionManager()

    def __str__(self):
        return self.title


class VideoManager(models.Manager):
    def instantiate_from_url(self, url: str, **kwargs):
        options = {"logger": logger}

        with youtube_dl.YoutubeDL(options) as ydl:
            info = ydl.extract_info(url, download=False)

        return self.model(
            title=info["title"], author=info["uploader"], url=url, **kwargs
        )

    def create_from_url(self, url: str, **kwargs):
        model = self.instantiate_from_url(url=url, **kwargs)
        model.save()

        return model


class Video(models.Model):
    url = models.URLField("URL")
    title = models.TextField()
    author = models.TextField()
    user = models.ForeignKey(User, related_name="videos", on_delete=models.CASCADE)
    task_id = models.TextField("Task ID", editable=False)

    objects = VideoManager()

    def __str__(self):
        return self.title
