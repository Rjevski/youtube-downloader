from django.core.management import BaseCommand

from videos.models import Subscription
from videos.services import refresh_subscription


class Command(BaseCommand):
    help = 'Refresh all subscriptions.'

    def handle(self, *args, **options):
        for subscription in Subscription.objects.all():
            refresh_subscription(subscription)
