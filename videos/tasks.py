import os.path
import tempfile

import youtube_dl
from celery import shared_task
from celery.utils.log import get_task_logger
from django.conf import settings
from django.core.mail import send_mail

from storage.sessions import UploadSession
from videos.models import Video

logger = get_task_logger(__name__)


@shared_task(bind=True)
def download(self, video_id: int):
    video = Video.objects.get(id=video_id)

    def download_progress_hook(progress_info):
        self.update_state(state="DOWNLOADING", meta=progress_info)

        # Youtube-DL reports the filename in its progress report, so grab it from there
        if "filename" in progress_info:
            self.target_path = progress_info["filename"]

    def upload_progress_hook(progress_info):
        self.update_state(state="UPLOADING", meta=progress_info)

    with tempfile.TemporaryDirectory() as download_directory:
        options = {
            "outtmpl": os.path.join(
                download_directory, "%(uploader)s-%(title)s-%(id)s.%(ext)s"
            ),
            "logger": logger,
            "progress_hooks": [download_progress_hook],
        }

        logger.info("Downloading video %s", video.url)

        with youtube_dl.YoutubeDL(options) as ydl:
            metadata = ydl.extract_info(video.url, download=False)
            ydl.download([video.url])

        with UploadSession(
            path=os.path.join("/", os.path.basename(self.target_path)),
            size=os.path.getsize(self.target_path),
            session=video.user.onedrive.session,
        ) as upload_session, open(self.target_path, "rb") as temp_file:

            upload_session.progress_callbacks.append(upload_progress_hook)
            logger.info("Uploading video %s", video.url)

            while True:
                chunk = temp_file.read(4000)

                if not chunk:
                    break

                upload_session.write(chunk)

            share_link = upload_session.share(type="view", scope="anonymous")["link"][
                "webUrl"
            ]
            logger.info("Share link: %s", share_link)

    uploader = metadata["uploader"]
    title = metadata["title"]
    description = metadata["description"]

    video.user.email_user(
        subject=f"{uploader} - {title}",
        message=f"{description}\n\n{share_link}\n\n(sent from YouTube downloader)",
        from_email=settings.EMAIL_FROM_ADDRESS,
    )
