from celery.result import AsyncResult
from django.contrib import admin
from django.utils.translation import ngettext

from videos.models import Subscription, Video
from videos.services import download_video, refresh_subscription


# Register your models here.

class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ['title', 'last_checked']
    actions = ['refresh']
    exclude = ['user']

    def get_exclude(self, request, obj=None):
        excludes = super().get_exclude(request, obj) or []

        if obj is None:
            return excludes + ['title']

        return excludes

    def save_form(self, request, form, change):
        if not change:
            form.instance = self.model.objects.instantiate_from_url(
                url=form.cleaned_data['url'],
                user=request.user
            )

        return super().save_form(request, form, change)

    def refresh(self, request, queryset):
        subscriptions = queryset.all()

        for subscription in subscriptions:
            refresh_subscription(subscription)

        message = ngettext(
            '{count} {name} was refreshed.',
            '{count} {name} were refreshed.',
            len(subscriptions)
        ).format(
            count=len(subscriptions),
            name='subscription' if len(subscriptions) == 1 else 'subscriptions'
        )

        self.message_user(request, message)


admin.site.register(Subscription, SubscriptionAdmin)


class VideoAdmin(admin.ModelAdmin):
    list_display = ['title', 'author', 'status', 'progress']
    actions = ['download']
    exclude = ['user']

    def get_exclude(self, request, obj=None):
        excludes = super().get_exclude(request, obj) or []

        if obj is None:
            return excludes + ['title', 'author', 'task_id']

        return excludes

    def save_form(self, request, form, change):
        if not change:
            form.instance = self.model.objects.instantiate_from_url(
                url=form.cleaned_data['url'],
                user=request.user
            )

        return super().save_form(request, form, change)

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)

        if not change:
            download_video(obj)

    def delete_model(self, request, obj):
        if obj.task_id is not None:
            task = AsyncResult(obj.task_id)
            task.revoke()

        return super().delete_model(request, obj)

    def delete_queryset(self, request, queryset):
        for obj in queryset.all():
            if obj.task_id is not None:
                task = AsyncResult(obj.task_id)
                task.revoke()

        return super().delete_queryset(request, queryset)

    def download(self, request, queryset):
        videos = queryset.all()

        for video in videos:
            download_video(video)

        message = ngettext(
            '{count} {name} was queued for download.',
            '{count} {name} were queued for download.',
            len(videos)
        ).format(
            count=len(videos),
            name='video' if len(videos) == 1 else 'videos'
        )

        self.message_user(request, message)

    @staticmethod
    def status(obj):
        task = AsyncResult(obj.task_id)
        return task.status

    @staticmethod
    def progress(obj):
        task = AsyncResult(obj.task_id)

        if type(task.info) is dict:
            if '_percent_str' in task.info:
                return task.info['_percent_str']

        return 'n/a'


admin.site.register(Video, VideoAdmin)
