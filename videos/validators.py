import re

from django.core.exceptions import ValidationError

CHANNEL_RE = re.compile(r"https?:\/\/(\w+.)?youtube.com\/channel\/([\w-]*)(\/.*)?$")
USER_RE = re.compile(r"https?:\/\/(\w+.)?youtube.com\/user\/(\w*)(\/.*)?$")


def validate_youtube_url(url):
    channel_match = CHANNEL_RE.match(url)

    if channel_match:
        return (
            "https://www.youtube.com/feeds/videos.xml?channel_id="
            + channel_match.group(2)
        )

    user_match = USER_RE.match(url)

    if user_match:
        return "https://www.youtube.com/feeds/videos.xml?user=" + user_match.group(2)

    raise ValidationError("Invalid URL, should be a channel or user URL.")
